﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Essential.Utils
{
    public static class UIHelpers
    {
        public static GameObject AddImageOnGO(GameObject _go)
        {
            if(_go.GetComponent<Image>() == null)
            {
                _go.AddComponent<Image>();
            }

            return _go;
        }

        // Overrided to add loading img from bundle
        public static GameObject AddImageOnGO(GameObject _go, AtlasLoader atlas, string ressource)
        {
            _go = AddImageOnGO(_go);

            Sprite sprite = atlas.getAtlas(ressource);
            _go.GetComponent<Image>().sprite = sprite;

            return _go;
        }
    }
}
