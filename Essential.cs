﻿using BepInEx;
using HarmonyLib;
using Essential.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Essential
{
    [BepInPlugin(Constant.MOD_NAME, Constant.MOD_BIO, Constant.MOD_VERSION)]
    class Essential : BaseUnityPlugin
    {
        void Awake()
        {
            Harmony.CreateAndPatchAll(Assembly.GetExecutingAssembly(), null);

            UnityEngine.Debug.Log("Harmony Patched content !");

            InitManagers();

            UnityEngine.Debug.Log("Managers intiated !");
        }

        void InitManagers()
        {
            AssetsBundleManager.Instance.Init(); // Load all bundles (bundle is containing few thins, textures, sounds, fbx, ...)
            MenuManager.Instance.Init();
            InputManager.Instance.Init();
        }
    }
}
