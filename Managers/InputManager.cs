﻿using Essential.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Essential.Managers
{
    public sealed class InputManager : IManager
    {
        #region Singleton
            private static InputManager instance = null;
            private static readonly object padlock = new object();

            InputManager()
            {
            }

            public static InputManager Instance
            {
                get
                {
                    lock (padlock)
                    {
                        if (instance == null)
                        {
                            instance = new InputManager();
                        }
                        return instance;
                    }
                }
            }
        #endregion

        public void Init()
        {

        }

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.Equals))
            {
                
            }
        }

        // Return true if menu was open when you press escape
        public bool EscapePressed()
        {
            return false;
        }
    }
}
