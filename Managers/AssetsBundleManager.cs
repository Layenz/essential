﻿using Essential.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Essential.Managers
{

    public sealed class AssetsBundleManager : IManager
    {
        #region Singleton
            private static AssetsBundleManager instance = null;
            private static readonly object padlock = new object();

            AssetsBundleManager()
            {
            }

            public static AssetsBundleManager Instance
            {
                get
                {
                    lock (padlock)
                    {
                        if (instance == null)
                        {
                            instance = new AssetsBundleManager();
                        }
                        return instance;
                    }
                }
            }
        #endregion

        List<string> _assetsList = new List<string> {

        };

        Dictionary<string, AssetBundle> _loadedAssets = new Dictionary<string, AssetBundle>();

        public void Init()
        {
            this.UnloadAllBundle();

            foreach(string bundleName in this._assetsList)
            {
                AssetBundle bundle = Utils.Utils.GetAssetBundleByName(bundleName);
                if (bundle != null)
                {
                    this._loadedAssets.Add(bundleName, bundle);
                }
            }
        }

        public void LoadBundle(string name)
        {
            bool toAdd = true;

            foreach (KeyValuePair<string, AssetBundle> entry in this._loadedAssets)
            {
                if (entry.Key == name)
                {
                    toAdd = false;
                    return; // Le bundle est déja chargé
                }
            }

            if (toAdd)
            {
                AssetBundle bundle = Utils.Utils.GetAssetBundleByName(name);
                if (bundle != null)
                {
                    this._loadedAssets.Add(name, bundle);
                }
            }
        }

        public bool BundleExist(string name)
        {
            return this._loadedAssets.ContainsKey(name);
        }

        public void UnloadBundle(string name, bool forceUnload = false)
        {
            bool isRemoved = false;
            foreach (KeyValuePair<string, AssetBundle> entry in this._loadedAssets)
            {
                if (entry.Key == name)
                {
                    entry.Value.Unload(forceUnload);
                    isRemoved = true;
                }
            }

            if (isRemoved)
            {
                this._loadedAssets.Remove(name);
            }
        }

        public AssetBundle GetBundleByName(string name)
        {
            if (this.BundleExist(name))
            {
                foreach (KeyValuePair<string, AssetBundle> entry in this._loadedAssets)
                {
                    if (entry.Key == name)
                    {
                        return entry.Value;
                    }
                }
            }
            else
            {
                this.LoadBundle(name);
                foreach (KeyValuePair<string, AssetBundle> entry in this._loadedAssets)
                {
                    if (entry.Key == name)
                    {
                        return entry.Value;
                    }
                }
            }

            return null;
        }

        public void UnloadAllBundle(bool forceUnload = false)
        {
            List<string> tempName = new List<string>();
            foreach(KeyValuePair<string, AssetBundle> entry in this._loadedAssets)
            {
                entry.Value.Unload(forceUnload);
                tempName.Add(entry.Key);
            }

            foreach(string name in tempName)
            {
                this._loadedAssets.Remove(name);
            }
        }
    }
}
