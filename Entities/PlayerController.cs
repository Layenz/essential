﻿using HarmonyLib;
using Essential.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Essential.Entities
{
    [HarmonyPatch(typeof(PlayerController), "FixedUpdate")]
    class ApplyPlayerController
    {
        static void Prefix(PlayerController __instance)
        {
            InputManager.Instance.Update();
            
        }
    } 
}
