﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Essential
{
    public static class Constant
    {
        public const string MOD_NAME = "org.bepinex.plugins.ModDefault";
        public const string MOD_BIO = "Exemple Test";
        public const string MOD_VERSION = "1.0.0.0";

        public const string GAME_DATA_PATH = "valheim_Data";
        public const string MOD_INSTALL_PATH = "BepInEx/plugins/ModDefault/";
    }
}
