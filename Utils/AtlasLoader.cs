﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Essential.Utils
{
    public class AtlasLoader
    {
        public Dictionary<string, Sprite> spriteDic = new Dictionary<string, Sprite>();

        //Creates new Instance only, Manually call the loadSprite function later on 
        public AtlasLoader()
        {

        }

        //Creates new Instance and Loads the provided sprites
        public AtlasLoader(AssetBundle myLoadedAssetBundle)
        {
            loadSprite(myLoadedAssetBundle);
        }

        //Loads the provided sprites
        public void loadSprite(AssetBundle myLoadedAssetBundle)
        {
            Sprite[] allSprites = myLoadedAssetBundle.LoadAllAssets<Sprite>();
            if (allSprites == null || allSprites.Length <= 0)
            {
                Debug.LogError("The Provided Base-Atlas Sprite does not exist!");
                return;
            }

            for (int i = 0; i < allSprites.Length; i++)
            {
                if (!spriteDic.ContainsKey(allSprites[i].name))
                {
                    spriteDic.Add(allSprites[i].name, allSprites[i]);
                }
            }
        }

        //Get the provided atlas from the loaded sprites
        public Sprite getAtlas(string atlasName)
        {
            Sprite tempSprite;

            if (!spriteDic.TryGetValue(atlasName, out tempSprite))
            {
                Debug.LogError("The Provided atlas `" + atlasName + "` does not exist!");
                return null;
            }
            return tempSprite;
        }

        //Returns number of sprites in the Atlas
        public int atlasCount()
        {
            return spriteDic.Count;
        }
    }
}
