﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Essential.Utils
{
    public static class Utils
    {
        //assetsvalheim
        public static AssetBundle GetAssetBundleByName(string bundleName)
        {
            string path = Application.dataPath;
            
            path = path.Replace(Constant.GAME_DATA_PATH, "");

            AssetBundle bundle = AssetBundle.LoadFromFile(Path.Combine(path, Constant.MOD_INSTALL_PATH + bundleName));

            if (bundle != null) {
                return bundle;
            } 
            else
            {
                return null;
            }
        }
    }
}
