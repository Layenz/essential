﻿using HarmonyLib;
using Essential.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModDefault.Entities
{
    [HarmonyPatch(typeof(Menu), "Update")]
    class ApplyMenu
    {
        static void Postfix(Menu __instance)
        {
            if (InputManager.Instance.EscapePressed())
            {
                __instance.m_root.gameObject.SetActive(false);
            }
        }
    }
}
