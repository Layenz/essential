﻿using Essential.Interfaces;
using Essential.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Essential.Managers
{
    public sealed class MenuManager : IManager
    {
        #region Singleton
            private static MenuManager instance = null;
            private static readonly object padlock = new object();

            MenuManager()
            {
            }

            public static MenuManager Instance
            {
                get
                {
                    lock (padlock)
                    {
                        if (instance == null)
                        {
                            instance = new MenuManager();
                        }
                        return instance;
                    }
                }
            }
        #endregion


        public void Init()
        {
            
        }

  
    }
}
